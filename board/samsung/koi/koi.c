/*
 * Copyright (C) 2013 Samsung Electronics
 *
 * See file CREDITS for list of people who contributed to this
 * project.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307 USA
 */

#include <common.h>
#include <asm/io.h>
#include <netdev.h>
#include <asm/arch/cpu.h>
#include <asm/arch/clk.h>
#include <asm/arch/clock.h>
#include <asm/arch/power.h>
#include <asm/arch/gpio.h>
#include <asm/arch/mmc.h>
#include <asm/arch/pinmux.h>
#include <asm/arch/sromc.h>
#include <asm/arch/sysreg.h>
#include "pmic.h"
#include <fastboot.h>

#define DEBOUNCE_DELAY	10000
#define OM_1stSDMMC_2ndUSB	0x2

#define MAX_GPT_PART		8

static unsigned long part_blknum[MAX_GPT_PART] = {
	CONFIG_GPT_RECOVERY_BLKNUM,
	CONFIG_GPT_SYSTEM_BLKNUM,
	CONFIG_GPT_USERDATA_BLKNUM,
	CONFIG_GPT_CACHE_BLKNUM,
	CONFIG_GPT_MISC_BLKNUM,
	CONFIG_GPT_BOOT_BLKNUM,
	CONFIG_GPT_INFO_BLKNUM,
	CONFIG_GPT_BOOTLOADER_BLKNUM
};

#define	DEV_NUM 0

DECLARE_GLOBAL_DATA_PTR;
unsigned int OmPin;

int board_init(void)
{
	u8 read_vol_arm;
	u8 read_vol_int, read_vol_g3d;
	u8 read_vol_mif, read_rtc_ctrl;
	u8 read_buck;
	u8 temp;
	unsigned int reg;

	char bl1_version[9] = {0};

	/* display BL1 version */
#ifdef CONFIG_TRUSTZONE_ENABLE
	printf("\nTrustZone Enabled BSP");
	strncpy(&bl1_version[0], (char *)(CONFIG_PHY_IRAM_NS_BASE + 0x810), 8);
	printf("\nBL1 version: %s\n", &bl1_version[0]);
#endif

#if defined(CONFIG_PM)
#if defined (CONFIG_PMIC_S5M8767A)
#if 0
	IIC0_ERead(S5M8767A_RD_ADDR, BUCK2_DVS, &read_vol_arm);
	IIC0_ERead(S5M8767A_RD_ADDR, BUCK3_DVS, &read_vol_int);
	IIC0_ERead(S5M8767A_RD_ADDR, BUCK4_DVS, &read_vol_g3d);

	printf("\nPMIC: 8767 + FAN3555\n ");
	printf("MIF--FAN3555, fixed to: %dmV\t\n", 1100);

	//IIC0_ERead(S5M8767A_RD_ADDR, BUCK2_CTRL, &temp);
	//printf("ARM---BUCK2, BUCK2_CTRL value: %d\t\n", temp);
	printf("ARM---BUCK2: %dmV\t\n", RD_BUCK_VOLT((unsigned int)read_vol_arm));

	//IIC0_ERead(S5M8767A_RD_ADDR, BUCK3_CTRL, &temp);
	//printf("INT---BUCK3, BUCK3_CTRL value: %d\t\n", temp);
	printf("INT---BUCK3: %dmV\t\n", RD_BUCK_VOLT((unsigned int)read_vol_int));

	//IIC0_ERead(S5M8767A_RD_ADDR, BUCK4_CTRL, &temp);
	//printf("G3D---BUCK4, BUCK4_CTRL value: %d\t\n", temp);
	printf("G3D---BUCK4: %dmV\t\n", RD_BUCK_VOLT((unsigned int)read_vol_g3d));
#endif
#endif
#if defined (CONFIG_PMIC_S2MPU02)

	IIC0_ERead(S2MPU02_RD_ADDR, BUCK1_CTRL2, &read_vol_mif);
	IIC0_ERead(S2MPU02_RD_ADDR, BUCK2_CTRL2, &read_vol_arm);
	IIC0_ERead(S2MPU02_RD_ADDR, BUCK3_CTRL2, &read_vol_int);
	IIC0_ERead(S2MPU02_RD_ADDR, BUCK4_CTRL2, &read_vol_g3d);
	printf("MIF: %dmV\t", RD_BUCK_VOLT((unsigned int)read_vol_mif));
	printf("ARM: %dmV\t", RD_BUCK_VOLT((unsigned int)read_vol_arm));
	printf("INT: %dmV\t", RD_BUCK_VOLT((unsigned int)read_vol_int));
	printf("G3D: %dmV\t\n", RD_BUCK_VOLT((unsigned int)read_vol_g3d));
#endif
#endif

	//gd->bd->bi_arch_number = MACH_TYPE_SMDK3250;

	gd->bd->bi_boot_params = (PHYS_SDRAM_1+0x100);

	OmPin = __REG(EXYNOS4_POWER_BASE + INFORM3_OFFSET);
	#define SDMMC_DEV		0x4
	unsigned int om_status = readl(EXYNOS4_POWER_BASE + OM_STATUS_OFFSET);
	if(om_status == SDMMC_DEV) {
		OmPin = BOOT_EMMC_4_4;
	}

	printf("\nChecking Boot Mode ...");
	if (OmPin == BOOT_ONENAND) {
		printf(" OneNand\n");
	} else if (OmPin == BOOT_NAND) {
		printf(" NAND\n");
	} else if (OmPin == BOOT_MMCSD) {
		printf(" SDMMC\n");
	} else if (OmPin == BOOT_EMMC) {
		printf(" EMMC4.3\n");
	} else if (OmPin == BOOT_EMMC_4_4) {
		printf(" EMMC4.41\n");
	} else {
		printf(" Please check OM_pin\n");
	}

	return 0;
}

int dram_init(void)
{
	gd->ram_size	= get_ram_size((long *)PHYS_SDRAM_1, PHYS_SDRAM_1_SIZE)
			+ get_ram_size((long *)PHYS_SDRAM_2, PHYS_SDRAM_2_SIZE)
			+ get_ram_size((long *)PHYS_SDRAM_3, PHYS_SDRAM_3_SIZE)
			+ get_ram_size((long *)PHYS_SDRAM_4, PHYS_SDRAM_4_SIZE)
			+ get_ram_size((long *)PHYS_SDRAM_5, PHYS_SDRAM_7_SIZE)
			+ get_ram_size((long *)PHYS_SDRAM_6, PHYS_SDRAM_7_SIZE)
			+ get_ram_size((long *)PHYS_SDRAM_7, PHYS_SDRAM_7_SIZE)
			+ get_ram_size((long *)PHYS_SDRAM_8, PHYS_SDRAM_8_SIZE);

	return 0;
}

void dram_init_banksize(void)
{
	gd->bd->bi_dram[0].start = PHYS_SDRAM_1;

	gd->bd->bi_dram[0].size = get_ram_size((long *)PHYS_SDRAM_1,
							PHYS_SDRAM_1_SIZE);
	gd->bd->bi_dram[1].start = PHYS_SDRAM_2;
	gd->bd->bi_dram[1].size = get_ram_size((long *)PHYS_SDRAM_2,
							PHYS_SDRAM_2_SIZE);
	gd->bd->bi_dram[2].start = PHYS_SDRAM_3;
	gd->bd->bi_dram[2].size = get_ram_size((long *)PHYS_SDRAM_3,
							PHYS_SDRAM_3_SIZE);
	gd->bd->bi_dram[3].start = PHYS_SDRAM_4;
	gd->bd->bi_dram[3].size = get_ram_size((long *)PHYS_SDRAM_4,
							PHYS_SDRAM_4_SIZE);
	gd->bd->bi_dram[4].start = PHYS_SDRAM_5;
	gd->bd->bi_dram[4].size = get_ram_size((long *)PHYS_SDRAM_5,
							PHYS_SDRAM_5_SIZE);
	gd->bd->bi_dram[5].start = PHYS_SDRAM_6;
	gd->bd->bi_dram[5].size = get_ram_size((long *)PHYS_SDRAM_6,
							PHYS_SDRAM_6_SIZE);
	gd->bd->bi_dram[6].start = PHYS_SDRAM_7;
	gd->bd->bi_dram[6].size = get_ram_size((long *)PHYS_SDRAM_7,
							PHYS_SDRAM_7_SIZE);
	gd->bd->bi_dram[7].start = PHYS_SDRAM_8;
	gd->bd->bi_dram[7].size = get_ram_size((long *)PHYS_SDRAM_8,
							PHYS_SDRAM_8_SIZE);
}

int board_eth_init(bd_t *bis)
{
	return 0;
}

#ifdef CONFIG_DISPLAY_BOARDINFO
int checkboard(void)
{

	printf("\nBoard: KOI\n");

	return 0;
}
#endif

#ifdef CONFIG_GENERIC_MMC
int board_mmc_init(bd_t *bis)
{
	struct exynos4_power *pmu = (struct exynos4_power *)EXYNOS4_POWER_BASE;
	int err, OmPin;

	OmPin = readl(&pmu->inform3);

	err = exynos_pinmux_config(PERIPH_ID_SDMMC2, PINMUX_FLAG_NONE);
	if (err) {
		debug("SDMMC2 not configured\n");
		return err;
	}

	err = exynos_pinmux_config(PERIPH_ID_SDMMC0, PINMUX_FLAG_8BIT_MODE);
	if (err) {
		debug("MSHC0 not configured\n");
		return err;
	}
#ifdef CONFIG_AUTO_FW_WRITE
#ifdef USE_MMC0
	set_mmc_clk(PERIPH_ID_SDMMC0, 0);
	err = s5p_mmc_init(PERIPH_ID_SDMMC0, 8);
#endif
#ifdef USE_MMC2
	set_mmc_clk(PERIPH_ID_SDMMC2, 0);
	err = s5p_mmc_init(PERIPH_ID_SDMMC2, 4);
#endif
#else
	if (OmPin == BOOT_EMMC_4_4 || OmPin == BOOT_EMMC) {
#ifdef USE_MMC0
		set_mmc_clk(PERIPH_ID_SDMMC0, 0);
		err = s5p_mmc_init(PERIPH_ID_SDMMC0, 8);
#endif
#ifdef USE_MMC2
		set_mmc_clk(PERIPH_ID_SDMMC2, 0);
		err = s5p_mmc_init(PERIPH_ID_SDMMC2, 4);
#endif
	}
	else {
#ifdef USE_MMC2
		set_mmc_clk(PERIPH_ID_SDMMC2, 0);
		err = s5p_mmc_init(PERIPH_ID_SDMMC2, 4);
#endif
#ifdef USE_MMC0
		set_mmc_clk(PERIPH_ID_SDMMC0, 0);
		err = s5p_mmc_init(PERIPH_ID_SDMMC0, 8);
#endif
	}
#endif
	return err;
}
#endif

static int board_uart_init(void)
{
	int err;

	err = exynos_pinmux_config(PERIPH_ID_UART2, PINMUX_FLAG_NONE);
	if (err) {
		debug("UART2 not configured\n");
		return err;
	}

	return 0;
}

#ifdef CONFIG_BOARD_EARLY_INIT_F
int board_early_init_f(void)
{
	return board_uart_init();
}
#endif

#if defined(CONFIG_EMERGENCY_MODE) || defined(CONFIG_FASTBOOT_MODE)
#define SCAN_KEY_CNT	10
unsigned int check_key(void)
{
	unsigned int scan_cnt = 0;
	unsigned int cur_key_val = 0;
	unsigned int old_key_val = 0;
	unsigned int total_scan_cnt = 0;
	int err;
	struct exynos3_gpio_part2 *gpio2 = (struct exynos3_gpio_part2 *)samsung_get_base_gpio_part2();

	unsigned int pwr, home, back;

	/* GPX1_7 : Volume Up */
	s5p_gpio_direction_input(&gpio2->x1, 7);
	s5p_gpio_set_pull(&gpio2->x1, 7, GPIO_PULL_UP);

	/* GPX2_7 : Power Key */
	s5p_gpio_direction_input(&gpio2->x2, 7);
	s5p_gpio_set_pull(&gpio2->x2, 7, GPIO_PULL_UP);

	/* GPX0_0 : Volume Down */
	s5p_gpio_direction_input(&gpio2->x0, 0);
	s5p_gpio_set_pull(&gpio2->x0, 0, GPIO_PULL_UP);

	/* Fix me : Add the code to scan key */

	mdelay(10);

	old_key_val = cur_key_val = s5p_gpio_get_value(&gpio2->x2, 7) << 2 |
				s5p_gpio_get_value(&gpio2->x0, 0) << 1 |
				s5p_gpio_get_value(&gpio2->x1, 7);

	while (1) {
		if (total_scan_cnt >= SCAN_KEY_CNT) {
			cur_key_val = 7;
			break;
		}

		mdelay(10);

		cur_key_val = s5p_gpio_get_value(&gpio2->x2, 7) << 2 |
					s5p_gpio_get_value(&gpio2->x0, 0) << 1 |
					s5p_gpio_get_value(&gpio2->x1, 7);

		if (cur_key_val == old_key_val) {
			scan_cnt++;
		} else {
			scan_cnt = 0;
			old_key_val = cur_key_val;
			total_scan_cnt++;
		}

		if (scan_cnt >= SCAN_KEY_CNT) {
			break;
		}
	}

	return cur_key_val ^ 7;	/* low->high active */
}
#endif


static void set_dieid(char *buf)
{
	char dieid[20] = {0};

	strncat(&dieid, buf, 16);
	setenv("dieid#", &dieid);
}

#define INFO_ENTRY_NUM	7

/*
 * return:
 *   0: all information was already written
 *   -1: empty
 *   1=<: information is insufficient
 */
static int get_bootarg_from_info_partition(char *buf)
{
	unsigned long start, count;
	unsigned char pid;
	int dev_num = 0;
	int tmp;
	char startstr[20];
	char arg[50];
	char *p;
	int entry_num = INFO_ENTRY_NUM;

	get_boot_part_info(dev_num, 7, &start, &count, &pid);
	if (pid != 0x83 && pid != 0xee)
		return -1;

	/*
		serial,0123456789abcdef
		btmac,22:22:27:39:1f:2f
		wifimac,f1:70:6e:81:7d:33
		mfprocno,123ab
		mfchkunit,1234567890ABCDE
		mfchkdate,20150818121035
		......
		.........
		.........................
		micgain,8f:a0
	*/

	strcpy(arg, "mmc read 0 0x48000000 ");
	sprintf(startstr, "0x%x ", start);
	strcat(arg, startstr);
	strcat(arg, "0x1");
	run_command(arg, 0);
	if (strncmp("serial", (char *)0x48000000, 6) == 0) {
		p = (char *)0x48000007;
		strcat(buf, " androidboot.serialno=");
		strncat(buf, p, 16);
		/* also set the die-id based on serial no */
		set_dieid(p);
		--entry_num;
	} else {
		return -1;
	}
	if (strncmp("btmac", (char *)0x48000018, 5) == 0) {
		p = (char *)0x4800001e;
		strcat(buf, " board_shiri_bluetooth.btmac=");
		strncat(buf, p, 17);
		--entry_num;
	}
	if (strncmp("wifimac", (char *)0x48000030, 7) == 0) {
		p = (char *)0x48000038;
		strcat(buf, " board_wifi_bcm.wifimac=");
		strncat(buf, p, 17);
		--entry_num;
	}
	if (strncmp("mfprocno", (char *)0x4800004a, 8) == 0) {
		--entry_num;
	}
	if (strncmp("mfchkunit", (char *)0x48000059, 9) == 0) {
		--entry_num;
	}
	if (strncmp("mfchkdate", (char *)0x48000073, 9) == 0) {
		--entry_num;
	}
	if (strncmp("micgain", (char *)0x480000b7, 7) == 0) {
		p = (char *)0x480000bf;
		strcat(buf, " snd_soc_ak4678.MicGain=");
		tmp = simple_strtoul(p, NULL, 16);
		sprintf(startstr, "%d", tmp);
		strcat(buf, startstr);
		p = (char *)0x480000c2;
		tmp = simple_strtoul(p, NULL, 16);
		sprintf(startstr, "%d", tmp);
		strcat(buf, " snd_soc_ak4678.IDVol=");
		strcat(buf, startstr);
		--entry_num;
	}
	return entry_num;
}

static int copy_info_partition(void)
{
	char arg[50];

	strcpy(arg, "mmc read 0 0x48000000 0x72AD10 0x1");
	run_command(arg, 0);
	if (strncmp("serial", (char *)0x48000000, 6) == 0) {
		strcpy(arg, "mmc write 0 0x48000000 0x72C000 0x1");
		run_command(arg, 0);
		return 0;
	}
	return -1;
}


static unsigned int mkbootimg_hdr[CFG_FASTBOOT_MKBOOTIMAGE_PAGE_SIZE/4];

static void get_bootarg_from_img_hdr(char *buf)
{
	struct fastboot_boot_img_hdr *fb_hdr = (struct fastboot_boot_img_hdr *)mkbootimg_hdr;

	if (strlen((char *)&fb_hdr->cmdline[0]) &&
		strstr((char *)&fb_hdr->cmdline[0], "=")) {
		strcat(buf, " ");
		strncat(buf, (char *)&fb_hdr->cmdline[0], strlen((char *)&fb_hdr->cmdline[0]));
	}
}

static void rw_img_hdr(int recovery, char *rw)
{
	char arg[50];
	char str[20];
	int partition_blk;

	sprintf(arg, "mmc %s 0 0x%x ", rw, mkbootimg_hdr);
	if (recovery)
		partition_blk = CONFIG_RECOVERY_PARTITION_BLK;
	else
		partition_blk = CONFIG_BOOT_PARTITION_BLK;
	sprintf(str, "0x%x 0x4", partition_blk);
	strcat(arg, str);
	run_command(arg, 0);
}

void check_update_bootloader(void)
{
	unsigned long start, count;
	unsigned char pid;
	int dev_num = 0;
	char startstr[20];
	char arg[50];

	get_boot_part_info(dev_num, 8, &start, &count, &pid);
	if (pid != 0x83 && pid != 0xee)
		return;

	strcpy(arg, "mmc read 0 0x48000000 ");
	sprintf(startstr, "0x%x ", start);
	strcat(arg, startstr);
	strcat(arg, "0x1000");
	run_command(arg, 0);

	if (memcmp(FASTBOOT_BOOTLOADER_MAGIC, (char *)0x48000000, 8) == 0) {
		write_bootloader_img((unsigned char *)0x48000000);
		/* zero-out the first byte of bootloader partition */
		memset((void *)0x48000000, 0, 4);
		strcpy(arg, "mmc write 0 0x48000000 ");
		sprintf(startstr, "0x%x ", start);
		strcat(arg, startstr);
		strcat(arg, "0x1");
		run_command(arg, 0);
	}
}

static int rw_raw_kernel(int recovery, unsigned char *buffer, char *rw)
{
	struct fastboot_boot_img_hdr *fb_hdr = (struct fastboot_boot_img_hdr *)mkbootimg_hdr;
	char arg[50];
	unsigned int addr, partition_blk, kernel_blknum;

	if (recovery)
		partition_blk = CONFIG_RECOVERY_PARTITION_BLK;
	else
		partition_blk = CONFIG_BOOT_PARTITION_BLK;
	partition_blk += 4;
	if (buffer)
		addr = (unsigned int)buffer;
	else
		addr = CONFIG_KERNEL_BASE_RAM_ADDR;
	kernel_blknum = (fb_hdr->kernel_size + CFG_FASTBOOT_SDMMC_BLOCKSIZE - 1)
					/ CFG_FASTBOOT_SDMMC_BLOCKSIZE;
	sprintf(arg, "mmc %s 0 0x%x 0x%x 0x%x", rw, addr, partition_blk, kernel_blknum);
	return(run_command(arg, 0));
}

static int rw_raw_ramdisk(int recovery, unsigned char *buffer, char *rw)
{
	struct fastboot_boot_img_hdr *fb_hdr = (struct fastboot_boot_img_hdr *)mkbootimg_hdr;
	char arg[50];
	unsigned int addr, partition_blk, ramdisk_offset, ramdisk_blknum;

	if (recovery)
		partition_blk = CONFIG_RECOVERY_PARTITION_BLK;
	else
		partition_blk = CONFIG_BOOT_PARTITION_BLK;
	partition_blk += 4;
	ramdisk_offset = ((fb_hdr->kernel_size + CFG_FASTBOOT_MKBOOTIMAGE_PAGE_SIZE - 1)
					/ CFG_FASTBOOT_MKBOOTIMAGE_PAGE_SIZE);
	partition_blk += ramdisk_offset * 4;
	if (buffer)
		addr = (unsigned int)buffer;
	else
		addr = CONFIG_RAMDISK_BASE_RAM_ADDR;

	ramdisk_blknum = (fb_hdr->ramdisk_size + CFG_FASTBOOT_SDMMC_BLOCKSIZE - 1)
						/ CFG_FASTBOOT_SDMMC_BLOCKSIZE;
	sprintf(arg, "mmc %s 0 0x%x 0x%x 0x%x", rw, addr, partition_blk, ramdisk_blknum);
	return(run_command(arg, 0));
}

void boot_kernel(int recovery)
{
	struct fastboot_boot_img_hdr *fb_hdr = (struct fastboot_boot_img_hdr *)mkbootimg_hdr;
	char ramdisk_size[32];

	/* write protect boot(u-boot, bl1, ...) partition */
	run_command("mmc_wp 0x0 1 0", 0);
	rw_raw_kernel(recovery, NULL, "read");
	rw_raw_ramdisk(recovery, NULL, "read");
	sprintf(ramdisk_size, "0x%x", fb_hdr->ramdisk_size);
	setenv("rootfslen", ramdisk_size);
	run_command(CONFIG_KERNEL_BOOT_CMD, 0);
}

int write_raw_ramdisk(unsigned char *buffer, unsigned size)
{
	struct fastboot_boot_img_hdr *fb_hdr = (struct fastboot_boot_img_hdr *)mkbootimg_hdr;
	int ret;
	
	fb_hdr->ramdisk_size = size;
	rw_img_hdr(0, "write");
	ret = rw_raw_ramdisk(0, buffer, "write");
	return ret;
}

int write_raw_kernel(unsigned char *buffer, unsigned size)
{
	struct fastboot_boot_img_hdr *fb_hdr = 	(struct fastboot_boot_img_hdr *)mkbootimg_hdr;
	int ret;
	
	rw_raw_ramdisk(0, CONFIG_WORK_RAM_ADDR, "read");
	fb_hdr->kernel_size = size;
	rw_img_hdr(0, "write");
	ret = rw_raw_kernel(0, buffer, "write");
	rw_raw_ramdisk(0, CONFIG_WORK_RAM_ADDR, "write");
	return ret;
}

static void show_selected(int num, int fin)
{
	run_command("lcdtextcolorbg 0 0 0", 0);
	if (num == 0) {
		run_command("lcdtextcolor 255 255 255", 0);
		if (fin)
			run_command("lcdtext 80 70 \"EXIT FASTBOOT\"", 0);
		else
			run_command("lcdtext 80 70 \"FASTBOOT MODE\"", 0);
		if (fin)
			run_command("lcdtextcolor 0 0 0", 0);	// erase
		else
			run_command("lcdtextcolor 80 80 80", 0);
		run_command("lcdtext \"RECOVERY MODE\"", 0);
	} else {
		if (fin)
			run_command("lcdtextcolor 0 0 0", 0);	// erase
		else
			run_command("lcdtextcolor 80 80 80", 0);
		run_command("lcdtext 80 70 \"FASTBOOT MODE\"", 0);
		run_command("lcdtextcolor 255 255 255", 0);
		run_command("lcdtext \"RECOVERY MODE\"", 0);
	}
}

static void wait_for_key_release(void)
{
	unsigned int key_val;

	while (1) {
		key_val = check_key();
		if (key_val == 0)
			break;
	}
}

int exit_key(void)
{
	unsigned int key_val;
	struct exynos3_gpio_part2 *gpio2 = (struct exynos3_gpio_part2 *)samsung_get_base_gpio_part2();

	key_val = s5p_gpio_get_value(&gpio2->x2, 7);	// power key
	key_val ^= 1;

	return key_val;
}

static int select_mode(void)
{
	int i = 0, init = 1;
	unsigned int key_val;
	int num = 0;

	show_selected(num, 0);

	wait_for_key_release();
	while (1) {
		key_val = check_key();
		if (key_val == 1 || // vol up
			key_val == 2) { // vol down
			num ^= 1;
			show_selected(num, 0);
		} else if(key_val == 4) {//power
			break;
		}
	}
	wait_for_key_release();
	return num;
}

static char tmp_buf[512];

int board_late_init(void)
{
	struct exynos4_power *pmu = (struct exynos4_power *)EXYNOS4_POWER_BASE;
	uint rst_stat;
	unsigned int pressed_key = ~0;
	unsigned om_status = readl(&pmu->om_stat) & 0x3f;
	char *reason;
	int mode;
	int ret;
	char *bootdelay;

	bootdelay = getenv("bootdelay");
	if (!(bootdelay && *bootdelay == '0')) {
		setenv("bootdelay", "0");
		saveenv_one_variable("bootdelay", "0");
	}

	rst_stat = readl(&pmu->rst_stat);
	printf("rst_stat : 0x%x\n", rst_stat);

	memset(tmp_buf,0,sizeof(tmp_buf));

	if(rst_stat & SYS_WDTRESET)
		reason = "watchdog";
	else if(readl(&pmu->inform4) == CONFIG_INFORM_PANIC)
		reason = "kernel_panic";
	else if(rst_stat & SWRESET)
		reason = "reboot";
	else if(rst_stat & PINRESET)
		reason = "pin_reset";
	else
		reason = "undefined";

	sprintf(tmp_buf, " androidboot.bootreason=%s androidboot.bootloader=%s ", 
		reason, VERSION_BOOTLOADER);
	strcat(tmp_buf, CONFIG_BOOTARGS);


#ifdef CONFIG_RECOVERY_MODE
	u32 second_boot_info = readl(CONFIG_SECONDARY_BOOT_INFORM_BASE);

	if (second_boot_info == 1) {
		printf("###Recovery Mode(SDMMC)###\n");
		writel(0x0, CONFIG_SECONDARY_BOOT_INFORM_BASE);
		setenv("bootcmd", CONFIG_RECOVERYCOMMAND_SDMMC);
	}
#ifdef CONFIG_EXYNOS_DA
	second_boot_info = readl(CONFIG_SECONDARY_BOOT_INFORM_BASE);
	if (second_boot_info == 2) {
		printf("###USB Download### om:%d\n", readl(&pmu->inform3));
		writel(0x0, CONFIG_SECONDARY_BOOT_INFORM_BASE);
		writel(BOOT_EMMC_4_4, &pmu->inform3);
		run_command(CFG_DNW_AUTO_CFG_PARTITION, 0); // make partition
		setenv("bootcmd", "dnw v05");
		printf("###USB Download### om:%d\n", readl(&pmu->inform3));
	}
#endif
#endif
	run_command("lcd 0", 0);
	run_command("lcd 2", 0);

	mode = readl(&pmu->inform4);
	if (mode == 0) {	// normal boot
		pressed_key = check_key();
		if (pressed_key & 1) {	// Volume up
			if (select_mode() == 0) {
				mode = CONFIG_FASTBOOT_MODE;
				show_selected(0, 1);
			} else {
				mode = CONFIG_FACTORY_RESET_MODE;
				show_selected(1, 1);
			}
		}
	} else if (mode == CONFIG_FASTBOOT_MODE) {
		show_selected(0, 1);
	}

#ifdef CONFIG_EFI_PARTITION
	if (OmPin == BOOT_MMCSD) {
	#ifdef CONFIG_AUTO_FW_WRITE
		printf("Start FW writing.\n");
		run_command("env default -f",0);
		run_command("run make_gpt_part",0);
		AutoFirmwareUpdate();
		run_command("poweroff", 0);
		while(1);
	#else
		printf("boot mode is SDMMC, skip GPT partitioning\n");
	#endif
	} else {
		int create_gpt = 0;
		if (find_gpt_part_name("recovery") != 0 
			|| find_gpt_part_name("boot") != 0
			|| find_gpt_part_name("info") != 0
			|| find_gpt_part_name("bootloader") != 0) {
			create_gpt = 1;
		} else {
			unsigned long start, count;
			unsigned char pid;
			int i;

			/* check if partitions are aligned with the partition
			 * information obtained from the partition table.
			 */
			for (i = 0; i < MAX_GPT_PART; i++) {
				get_boot_part_info(DEV_NUM, i+1, &start, &count, &pid);
				if (i == 0 && start != CONFIG_GPT_RECOVERY_START_BLK) {
					create_gpt = 1;
					break;
				} else if (pid != 0xee) {
					printf("Part%d: NOT GPT PARTITION ERROR !!\n", i);
					create_gpt = 1;
					break;
				} else if (count != part_blknum[i]) {
					printf("Part%d: Block Number differ!!\n", i);
					create_gpt = 1;
					break;
				}
			}
		}
		if (create_gpt) {
			run_command("env default -f",0);
			run_command("run make_gpt_part",0);
			printf("Creating gpt partitions!!\n");
		}
	}

	if ((ret = get_bootarg_from_info_partition(tmp_buf)) < 0) {
		if (copy_info_partition() == 0)	// copy from old location
			ret = get_bootarg_from_info_partition(tmp_buf);	// retry
	}
	if (ret == 0) {
		/* power-on write protect info partition */
		run_command("mmc_wp 0x72c000 1 1", 0);
	}
	rw_img_hdr(mode==CONFIG_FACTORY_RESET_MODE ? 1:0, "read");
	get_bootarg_from_img_hdr(tmp_buf);
	setenv("bootargs", tmp_buf);
#endif

	if (mode == CONFIG_FACTORY_RESET_MODE) {
		writel(CONFIG_UPDATE_RESET_MODE, &pmu->inform4);
		boot_kernel(1);
	}

	if (mode == CONFIG_UPDATE_RESET_MODE) {
		writel(0x0, &pmu->inform4);
		/* check if we need to update the bootloader as well */
		check_update_bootloader();

		run_command(CONFIG_UPDATE_RESET_COMMAND, 0);
	}



	if(mode == CONFIG_FASTBOOT_MODE) {
		writel(0x0, &pmu->inform4);
		printf("Download Mode \n");
		run_command("fastboot", 0);
		printf("board late init fastboot execute!!\n");
	}


#ifdef CONFIG_RAMDUMP_MODE
	/*   check reset status for ramdump */
	if ((rst_stat & (WRESET | SYS_WDTRESET | ISP_ARM_WDTRESET))
		|| (readl(CONFIG_RAMDUMP_SCRATCH) == CONFIG_RAMDUMP_MODE)) {
		/*   run fastboot */
		run_command("fastboot", 0);
	}
#endif
	return 0;
}
