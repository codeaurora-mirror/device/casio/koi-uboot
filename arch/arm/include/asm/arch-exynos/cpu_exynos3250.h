/*
 * (C) Copyright 2011 Samsung Electronics Co. Ltd
 *
 * See file CREDITS for list of people who contributed to this
 * project.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 */

#ifndef _EXYNOS3250_CPU_H
#define _EXYNOS3250_CPU_H

#define EXYNOS3250_POWER_BASE           0x10020000

/*
 * POWER
 */
#define ELFIN_POWER_BASE                EXYNOS3250_POWER_BASE

#define SW_RST_REG_OFFSET		0x400
#define SW_RST_REG_STAT_OFFSET	0x404
#define SW_RST_REG				__REG(EXYNOS3250_POWER_BASE + SW_RST_REG_OFFSET)
#define SW_RST_STAT_REG 		__REG(EXYNOS3250_POWER_BASE + SW_RST_REG_STAT_OFFSET)

/* Define Reset Status */
#define	SOFTWARE_RESET	29
#define	WARM_RESET	28
#define ISP_ARM_WDT_RESET	25
#define SYS_WDT_RESET	20
#define PINRESET_RESET	16

#endif	/* _EXYNOS3250_CPU_H */
